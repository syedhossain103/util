module gitlab.com/akita/util

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/mock v1.3.1
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.3.0 // indirect
	github.com/onsi/ginkgo v1.10.2
	github.com/onsi/gomega v1.5.0
	github.com/rs/xid v1.2.1
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/tebeka/atexit v0.1.0
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/akita/akita v1.10.0
	go.mongodb.org/mongo-driver v1.0.3
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
)

// replace gitlab.com/akita/akita => ../akita

go 1.13
